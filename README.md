# AWS DevRel program: Analyzing Open Distro Community

Repo initially created to develop my Data Science MSc thesis.

## Motivation

This project analyzes OpenDistro community, an open source project donated by Amazon Web Services (AWS). The main goal is to create a model which profiles different kinds of developers, based on the activity performed through different channels where the developer community interacts with Open Distro’s technology. This project aims to build a set of metrics that DevRel specialists can use to report their work through a community-centric model.
 
Collection, analysis and curation of data is carried out using GrimoireLab’s Perceval open source technology, a tool specialized in software development to produce data analysis and visualization. Data storage and visualization will be done through Elasticsearch, Kibana and Plotly, tools seen during the master.

The main motivation for this work arises from the need of the DevRel community to have a set of metrics and models capable of fully quantifying the value of their work.

This project has the domain knowledge provided by members from DevRel Collective, DevRelCon and DevRel Spain.

## Measuring DevRel success: Needs and pain points

One of the most difficult things to do in Developer Relations (DevRel) is prove that the investment is worthwhile. Main issues are related with developer community segmentation, KPIs adapted to devrel needs and powerful tools to analyze the right data where developers are (e.g Stackoverflow, github, slack, etc)

## Measuring DevRel success for Open Distro

Goal: “grow technology adoption” as a potential goal behind Open Distro’s DevRel strategy

## Data Sources

**What data sources are involved in Open Distro community?**

* Online events/ meetings: Meetup
* Tickets/Issue Systems: GitHub
* Social Media: Twitter
* Forums: Discourse

> We will only focus on analyzing the developer community from Open Distro. The platform Open Distro uses to make code contributions and feature requests in this case is GitHub. Thus, questions and metrics will focus on GitHub interactions.

**What kind of interactions can we measure from an open source developer community perspective in GitHub?**

* Issues: we can find open issues and closed issues.
* Pull Requests: we can find open PR, closed and not-merged or closed and merged PR 

## Questions and metrics:

Part 1: build a community segmentation model based on the type of activity performed

| | | Maintainer activity | Contributor activity | Contirbutor activity |
| ---- | ---- |--- | --- | --- |
| **Questioning Community type** | Segment 1:  "Questions"|  |  | Opened Issues |
| **Problem-solving Community type** | Segment 2: "Answers" | | Not-merged Pull Requests | Closed Issues |
| **Creator-Inventor Community type** | Segment 3: “Code” | Merged Pull Requests | | |


>[GitHub account](https://github.com/opendistro-for-elasticsearch)

Part 2: build a comprehensive set of metrics that answer questions alligned with open distro's goal

| Question | Metric | Field |
| ---- | ---- |--- |
| How much activity does Open Distro have? | Total number of issues and PR | Community activity |
| How much activity coming from non-OpenDistro Developers does Open Distro have? | Total number of issues and PR from third-party developers (external organizations) | Community activity |
| What is the activity evolution of non-OpenDistro Developers contributing to Open Distro? | issues and PR growth evolution from third-party developers (external organizations)| Community activity |
| How engaged is Open Distro community? What’s the shape of Open Distro’s community? | Percentage of open issues, closed issues, committed PRs, and merged PR contributions | Community engagement |
| How responsive is Open Distro community? | PR and Issue time to close: how long did it take from creation until response (merge or close) | Community Satisfaction |
| How responsive is Open Distro community over time? | PR and Issue speed time response over time (survival analysis curve) | Community Satisfaction |


## Python Scripts

In order to start with our analysis (data gathering and storage) we need 2 different resources available in the following GitLab public repo.

Python scripts: 

* Opendistro-github.py: contains extract_prs, extract_issues, transorm_item, extract_data, owner_repositories adn main() functions to gather and load data into an elasticsearch instance.
* Githubenriched.py: function needed to obtain time to close issues and pull requests ( in days) for each document.

Mapping.json: contains the schema to load the data into an elasticsearch instance and visualize it  through kibana and jupyter notebook.

There are certain queries we can’t perform with kibana or it takes a lot of effort to get them done. That’s why we will use elasticsearch dsl python library to interact with our elasticsearch index and create the desired queries. Special metrics can be divided into three scripts:

* special _metrics.py: script created to obtain our developer segments (questions, answers and code). This will be used again in a jupyter notebook to see the radar visualization using plotly.

* survivalanalysisPR.ipynb: queries needed to create the survival analysis curve for closed PR
* survivalanalysisissues.ipynb: queries needed to create the survival analysis curve for closed issues


## Running the script

opendistro-github.py python script is sending data directly through Elastic. Thus, we will need to have Elastic and Kibana running in our local machine:

````
user@user:~/elasticsearch-oss-7.6.2-linux-x86_64/elasticsearch-7.6.2$ ./bin/elasticsearch
````

````
user@user:~/kibana-oss-7.6.2-linux-x86_64/kibana-7.6.2-linux-x86_64$ ./bin/kibana
````

Once this is done, we can go to the folder where our script is running

````
anajs@anajsana:~/tfm-data-science/python scripts$ python opendistro-github.py
````
Once you run the script, we can see everything is on place by searching the elasticsearch index at `localhost:9200/_cat/indices`. We can then go to Kibana(`localhost:5601`) and create a new index with elasticsearch index, using creation_date field as time filter.




## How to contribute?

We can find three different kinds of [milestones](https://gitlab.com/anajimenezsantamaria/tfm-data-science/-/milestones). You can open a new issue assigned to any of the Milestones, to ask questions, add new documentation or request to fix bugs. For reature requests, fell free to open a merge request 

Please make sure to use [tags](https://gitlab.com/anajimenezsantamaria/tfm-data-science/-/labels) every time you create an issue or merge request.




