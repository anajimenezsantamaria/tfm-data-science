## Open Source tech used

| Technology | Description | Documentation |
| ---- | ---- | ---- |
| **Perceval /SortingHat** | Set of Python modules to retrieve and gather data from main software development data sources (GitHub, Git, Slack, etc) | https://perceval.readthedocs.io/en/latest/index.html |
| **Arrow** | Python module which allows you work with dates and times with fewer imports and a lot less code |https://arrow.readthedocs.io/en/stable/ |
| **Elasticsearch** | Distributed and RESTful Search Engine that can be used as a distributed NoSQL database | https://www.elastic.co/guide/index.html |
| **Kibana** | Data visualization dashboard for Elasticsearch | https://www.elastic.co/guide/en/kibana/index.html |
| **Plotly** |  Interactive graphing library for Python | https://plotly.com/python/ |
| **Jupyter notebook** | web application that allows to create and share documents that contain live code, equations, visualizations and narrative text | https://jupyter.org/documentation |
