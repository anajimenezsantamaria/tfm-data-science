from datetime import datetime


def str_to_datetime(tm):
    '''convirte formato string en datetime'''
    return datetime.strptime(tm, "%Y-%m-%dT%H:%M:%SZ")


def get_time_diff_days(start, end):
    ''' Number of days between two dates in UTC format  '''

    if start is None or end is None:
        return None

    if type(start) is not datetime:
        start = str_to_datetime(start).replace(tzinfo=None)
    if type(end) is not datetime:
        end = str_to_datetime(end).replace(tzinfo=None)

    seconds_day = float(60 * 60 * 24)
    diff_days = (end - start).total_seconds() / seconds_day
    # convierte diff_days en un float con dos decimales
    diff_days = float('%.2f' % diff_days)

    return diff_days