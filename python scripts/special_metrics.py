from elasticsearch import Elasticsearch, ElasticsearchException
from elasticsearch_dsl import Search, Q
from datetime import datetime
import logging

# te permite hacer loggins
logger = logging.getLogger(__name__)

INDEX = "index2_opendistro"

def open_issues(client, date):
    '''computes total open issues from a given date'''
    # lte = less than or equal / gte = greater than or equal
    s = Search(using=client, index=INDEX) \
        .query(Q('match', category='issue')) \
        .query(Q('range', created_at={'lte': date}) &
              (Q('range', closed_at={'gte': date}) | Q('match', state='open')))

    try:
        response = s.count()
    except ElasticsearchException as e:
        logger.warning(e)
        response = None

    return response

def total_pr_issues(client, date):
    s = Search(using=client, index=INDEX)

    try:
        response = s.count()
    except ElasticsearchException as e:
        logger.warning(e)
        response = None

    return response

def open_pr(client, date):
    s = Search(using=client, index=INDEX) \
        .query(Q('match', category='pull_request')) \
        .query(Q('range', created_at={'lte': date}) &
              (Q('range', closed_at={'gte': date}) | Q('match', state='open')))

    try:
        response = s.count()
    except ElasticsearchException as e:
        logger.warning(e)
        response = None

    return response

def closed_issues(client, date):
    s = Search(using=client, index=INDEX) \
        .query(Q('match', category='issue')) \
        .query(Q('range', closed_at={'lte': date}))

    try:
        response = s.count()
    except ElasticsearchException as e:
        logger.warning(e)
        response = None

    return response

def not_merged_pr(client, date):
    s = Search(using=client, index=INDEX) \
        .query(Q('match', category='pull_request')) \
        .query(Q('range', created_at={'lte': date}) &
              (Q('range', closed_at={'gte': date}) | Q('match', merged=False)))
    try:
        response = s.count()
    except ElasticsearchException as e:
        logger.warning(e)
        response = None

    return response


def merged_pr(client, date):
    s = Search(using=client, index=INDEX) \
        .query(Q('match', category='pull_request')) \
        .query(Q('range', created_at={'lte': date}) &
              (Q('range', closed_at={'gte': date}) | Q('match', merged=True)))
    try:
        response = s.count()
    except ElasticsearchException as e:
        logger.warning(e)
        response = None

    return response

def get_metrics(client, date):
    metrics = dict()
    metrics['open_issues'] = open_issues(client, date)
    metrics['total_pr_issues'] = total_pr_issues(client, date)
    metrics['open_pr'] = open_pr(client, date)
    metrics['closed_issues'] = closed_issues(client, date)
    metrics['not_merged_pr'] = not_merged_pr(client, date)
    metrics['merged_pr'] = merged_pr(client, date)
    metrics['sum'] = metrics['closed_issues'] + metrics['not_merged_pr']
    if None in [metrics['total_pr_issues'], metrics['open_issues']] or metrics['total_pr_issues'] == 0:
        metrics['questions'] = None
    else:
        metrics['questions'] = metrics['open_issues'] / metrics['total_pr_issues'] * 100

    if None in [metrics['total_pr_issues'], metrics['merged_pr']] or metrics['total_pr_issues'] == 0:
        metrics['code'] = None
    else:
        metrics['code'] = metrics['merged_pr'] / metrics['total_pr_issues'] * 100

    if None in [metrics['total_pr_issues'], metrics['sum']] or metrics['total_pr_issues'] == 0:
        metrics['answers'] = None
    else:
        metrics['answers'] = metrics['sum'] / metrics['total_pr_issues'] * 100

    return metrics


def main():
    client = Elasticsearch(["http://127.0.0.1:9200"])
    # pasas una fecha, la fecha actual
    now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    metrics = get_metrics(client, now)
    #las metricas seran retornadas en un dict
    for metric in metrics:
        #imprime la metrica y el valor de la metrica
        print(f'{metric}: {metrics[metric]}')


if __name__ == '__main__':
    main()