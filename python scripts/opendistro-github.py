import requests
import time
import json
import arrow
# helpers permite interaccionar con la API de bulk
from elasticsearch import Elasticsearch, helpers
import logging
from datetime import datetime # to specify a specific timeframe
from perceval.backends.core.github import GitHub
from githubenriched import get_time_diff_days

# Crear variable para que todas las funciones lo tengan en cuenta
INDEX_NAME = 'enriched_try'

ORGS = ['aws', 'opendistro-for-elasticsearch', 'opendistro']
MEMBER = ['member']

def extract_prs(repo):

    prs = []
    pr_repo = GitHub(owner="opendistro-for-elasticsearch", repository=repo, api_token=["xxxxxx"], sleep_for_rate=True)
    # .fetch generates issues
    n = 1
    for pr in pr_repo.fetch(category= 'pull_request', from_date=datetime.strptime('2020-01-01', '%Y-%m-%d')):#arrow.get('2020-01-01', 'YYYY-MM-DD')):
	# con f te ahorras el poner .format
        logging.info(f'Found new PR for {repo}:{n}')
        prs.append({
	  # hay que crear un campo index para que la api bulk sepa en qué indice meter los datos
          '_index': INDEX_NAME,
          '_source': {
            'category': pr['category'],
            'timestamp': pr['timestamp'],
            'url': pr['data']['url'],
            'merged': pr['data']['merged'],
            'state': pr['data']['state'],
            'created_at': pr['data']['created_at'],
            'closed_at': pr['data']['closed_at'],
            'merged_at': pr['data']['merged_at'],
            'repo': pr['search_fields']['repo'],
            'third-party-dev': not any([x for x in pr['data']['user_data']['organizations'] if x['login'] in ORGS]),
            'user': pr['data']['user']['login'],
            'association': pr['data']['author_association'] in MEMBER,
	    'author_association': pr['data']['author_association'],
            'login': pr['data']['user_data']['login'],
            'id': pr['data']['user_data']['id'],
            'avatar_url': pr['data']['user_data']['avatar_url'],
            'author_url': pr['data']['user_data']['url'],
            'html_url': pr['data']['user_data']['html_url'],
            'organizations_url': pr['data']['user_data']['organizations_url'],
            'repos_url': pr['data']['user_data']['repos_url'],
            'events_url': pr['data']['user_data']['events_url'],
            'type': pr['data']['user_data']['type'],
            'site_admin': pr['data']['user_data']['site_admin'],
            'name': pr['data']['user_data']['name'],
            'company': pr['data']['user_data']['company'],
            'location': pr['data']['user_data']['location'],
            'email': pr['data']['user_data']['email'],
            'organizations': [org['login'] for org in pr['data']['user_data']['organizations']],
	    'time_to_close_days': get_time_diff_days(pr['data']['created_at'], pr['data']['closed_at']),
            'time_to_merge_days': get_time_diff_days(pr['data']['created_at'], pr['data']['merged_at']),
	  }   
       })
       n += 1
    return prs

def extract_issues(repo):

    issues = []
    issue_repo = GitHub(owner="opendistro-for-elasticsearch", repository=repo, api_token=["xxxx"], sleep_for_rate=True)
    n = 1
    for issue in issue_repo.fetch(category= 'issue', from_date=datetime.strptime('2020-01-01', '%Y-%m-%d')):
	logging.info(f'Found new Issue for {repo}:{n}')
        issues.append({
	  '_index': INDEX_NAME,
          '_source': {
            'category': issue['category'],
            'timestamp': issue['timestamp'],
            'url': issue['data']['url'],
            'state': issue['data']['state'],
            'created_at': issue['data']['created_at'],
            'closed_at': issue['data']['closed_at'],
            'repo': issue['search_fields']['repo'],
            'third-party-dev': not any([x for x in issue['data']['user_data']['organizations'] if x['login'] in ORGS]),
            'user': issue['data']['user']['login'],
            'association': issue['data']['author_association'] in MEMBER,
	    'author_association': issue['data']['author_association'],
            'login': issue['data']['user_data']['login'],
            'id': issue['data']['user_data']['id'],
            'avatar_url': issue['data']['user_data']['avatar_url'],
            'author_url': issue['data']['user_data']['url'],
            'html_url': issue['data']['user_data']['html_url'],
            'organizations_url': issue['data']['user_data']['organizations_url'],
            'repos_url': issue['data']['user_data']['repos_url'],
            'events_url': issue['data']['user_data']['events_url'],
            'type': issue['data']['user_data']['type'],
            'site_admin': issue['data']['user_data']['site_admin'],
            'name': issue['data']['user_data']['name'],
            'company': issue['data']['user_data']['company'],
            'location': issue['data']['user_data']['location'],
            'email': issue['data']['user_data']['email'],
            # a list with names rom all items showing at organizations field is created (logins)
            'organizations': [org['login'] for org in issue['data']['user_data']['organizations']],
	    'time_to_close_days': get_time_diff_days(issue['data']['created_at'], issue['data']['closed_at']),
          }
       })
       n += 1
    return issues

def transform_item(item):
    if item['timestamp']:
       item['timestamp'] = arrow.get(item['timestamp']).format('YYYY-MM-DD HH:mm:ss')#datetime.fromtimestamp(item['timestamp']).strftime("%Y-%m-%d %H:%M:%S")
    if item['created_at']:
       item['created_at'] = arrow.get(item['created_at'], 'YYYY-MM-DDTHH:mm:ssZ').format('YYYY-MM-DD HH:mm:ss')#datetime.strptime(item['created_at'], "%Y-%m-%dT%H:%M:%SZ").strftime("%Y-%m-%d %H:%M:%S")
    if item['closed_at']:
       item['closed_at'] = arrow.get(item['closed_at'], 'YYYY-MM-DDTHH:mm:ssZ').format('YYYY-MM-DD HH:mm:ss')#datetime.strptime(item['closed_at'], "%Y-%m-%dT%H:%M:%SZ").strftime("%Y-%m-%d %H:%M:%S")
    # the first one checks i the key exists, the second one check if its value is not 'none'
    if 'merged_at' in item and item['merged_at']:
       item['merged_at'] = arrow.get(item['merged_at'], 'YYYY-MM-DDTHH:mm:ssZ').format('YYYY-MM-DD HH:mm:ss')#datetime.strptime(item['merged_at'], "%Y-%m-%dT%H:%M:%SZ").strftime("%Y-%m-%d %H:%M:%S")


def extract_data(repo):

  data = []
  # con extend se hace un merge de dos listas
  logging.info(f'Starting PRs gathering for {repo}')
  data.extend(extract_prs(repo))
  logging.info(f'Finished PRs gathering for {repo}')
  logging.info(f'Starting Issues gathering for {repo}')
  data.extend(extract_issues(repo))
  logging.info(f'Finished Issues gathering for {repo}')

  logging.info(f'Starting data transformation for {repo}')
  for item in data:
    # ahora el item esta dentro de source
    transform_item(item['_source'])
  logging.info(f'Finished data transformation for {repo}')

  return data


def owner_repositories(name):
    query = "org:{}".format(name)
    page = 1
    repositories = []

    r = requests.get('https://api.github.com/search/repositories?q={}&page={}'.format(query, page))
    items = r.json()['items']

    while len(items) > 0:
        # github API has rate limit. that's why you ask for a 3 sec inactivity
        time.sleep(3)
        for item in items:
            logging.info("Adding {} repository".format(item['clone_url']))
            repositories.append(item['name'])
        page += 1
        r = requests.get('https://api.github.com/search/repositories?q={}&page={}'.format(query, page))
        items = r.json()['items']

    return repositories

def main():
    
    # time.time te da el valor del instante actual. varía según cuando lo ejecutes.
    start_time_global = time.time()
    elastic = Elasticsearch()
    # Read mapping json file to ElasticSearch
    mapping_file = open("mapping.json", "r")
    mapping = json.load(mapping_file)
    # Creation of ElasticSearch index using the mapping
    index_name = INDEX_NAME
    response = elastic.indices.create(
        index=index_name,
        body=mapping,
        ignore=400  # ignore 400 already exists code
    )
    # Instance to log events on python terminal
    logging.basicConfig(level=logging.INFO)
    # Open in write mode a json document to save data from repository
    #data_file = open("datatry.json", "w")
    
    repos = owner_repositories('opendistro-for-elasticsearch')
    for repo in repos:
        logging.info(f'Starting data gathering for {repo}')
	start_time_repo = time.time()
        data = extract_data(repo)
	logging.info(f'{repo} took {time.time() - start_time_repo} seconds')
        start_time_elastic = time.time()
        helpers.bulk(elastic, data)
	logging.info(f'Bulk for {repo} took {time.time() - start_time_elastic} seconds')
        logging.info(f'Finished data gathering for {repo}')
        #break

    logging.info(f'Total spent time: {time.time() - start_time_global}')

if __name__ == '__main__':
    main()

